﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Population_Database_2
{
    class DB
    {
        const string CONNECT_STRING = @"Server=LAPTOP-9ATPBRN6\ASQL;Database=Population;Trusted_Connection=True;";
        SqlConnection conn;

        static DB _db;

        private DB()
        {
            conn = new SqlConnection(CONNECT_STRING);
            conn.Open();
        }

        public static DB GetInstance()
        {
            if (_db == null)
                _db = new DB();
            return _db;
        }

        public double MaxPopulation()
        {
            double maxPopulation = 0D;
            var cmdString = "SELECT MAX(Population) AS maxp FROM City";
            var cmd = new SqlCommand(cmdString, conn);
            SqlDataReader rd = cmd.ExecuteReader();
            if(rd.Read())
            {
                maxPopulation = rd.GetDouble(rd.GetOrdinal("maxp"));
                rd.Close();
                return maxPopulation;
            }
            else
            {
                rd.Close();
                return 0;
            }                      
        }

        public double MinPopulation()
        {
            double minPopulation = 0D;
            var cmdString = "SELECT MIN(Population) as min FROM City";
            var cmd = new SqlCommand(cmdString, conn);
            SqlDataReader rd = cmd.ExecuteReader();
            if(rd.Read())
            {
                minPopulation = rd.GetDouble(rd.GetOrdinal("min"));
                rd.Close();
                return minPopulation;
            }
            else
            {
                rd.Close();
                return 0;
            }            
        }

        public void Add(City city)
        {
            var cmdString = "INSERT INTO City " +
                                    "(City, Population)" +
                                    "VALUES" +
                                    "(@CITY, @POPULATION)";

            var cmd = new SqlCommand(cmdString, conn);
            cmd.Parameters.AddWithValue("@CITY", city.CityName);
            cmd.Parameters.AddWithValue("@POPULATION", city.Population);

            cmd.ExecuteNonQuery();
        }

        public void Delete(City city)
        {
            var cmdString = "DELETE City " +
                               "WHERE City = @CITY";

            var cmd = new SqlCommand(cmdString, conn);
            cmd.Parameters.AddWithValue("@CITY", city.CityName);

            cmd.ExecuteNonQuery();
        }
        
        public List<City> Get()
        {
            var cities = new List<City>();
            var cmdString = "SELECT City, Population" +
                               " FROM City";
            var cmd = new SqlCommand(cmdString, conn);
            SqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
                cities.Add(new City()
                {
                    CityName = rd.GetString(rd.GetOrdinal("City")),
                    Population = rd.GetDouble(rd.GetOrdinal("Population")),
                });
            rd.Close();

            return cities;
        }

        public void SortbyCityName(List<City> cities)
        {
            cities.Sort(new CitySortByCityName());
        }

        public void SortbyPopulation(List<City> cities)
        {
            cities.Sort(new CitySortByCityPopulation());
        }
    }
}
