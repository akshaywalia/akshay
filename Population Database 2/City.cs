﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Population_Database_2
{
    public class CitySortByCityName : IComparer<City>
    {
        public int Compare(City x, City y)
        {
            var res = x.CityName.CompareTo(y.CityName);
            return res;
        }
    }

    public class CitySortByCityPopulation : IComparer<City>
    {
        public int Compare(City x, City y)
        {
            var res = x.Population.CompareTo(y.Population);
            return (-res);
        }
    }

    public class City
    {
        public string CityName { get; set; }
        public double Population { get; set; }

        public City()
        {
            CityName = "";
            Population = 0;
        }

        public City(string cityname,float population)
        {            
            CityName = cityname;
            Population = population;
        }

        //make a copy of the object
        //use this to edit a copy in case of cancel
        public City Clone()
        {
            var c = new City
            {
                CityName = CityName,
                Population = Population,
            };
            return c;
        }

        public override string ToString() => $"{CityName} | {Population}";
    }
}


