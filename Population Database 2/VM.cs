﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Population_Database_2
{
    public class VM : INotifyPropertyChanged
    {
        //get a reference to the database object that we can use repeatedly
        DB db = DB.GetInstance();

        #region        
        List<City> cityList;

        //the list to show on the main page
        BindingList<City> cities;
        public BindingList<City> Cities 
        {
            get { return cities; }
            set { cities = value; }
        }
        //the selected item
        City selectedCity;
        public City SelectedCity
        {
            get { return selectedCity; }
            set { selectedCity = value; NotifyChanged(); }
        }
        //a copy of the selected item or a new item to be edited
        City cityToEdit;
        public City CityToEdit
        {
            get { return cityToEdit; }
            set { cityToEdit = value; NotifyChanged(); }
        }
        //property for lables
        private double totalPopulation = 0;
        public double TotalPopulation
        {
            get { return totalPopulation; }
            set { totalPopulation = value; NotifyChanged(); }
        }

        private double highestPopulation = 0;
        public double HighestPopulation
        {
            get { return highestPopulation; }
            set { highestPopulation = value; NotifyChanged(); }
        }

        private double lowestPopulation = 0;
        public double LowestPopulation
        {
            get { return lowestPopulation; }
            set { lowestPopulation = value; NotifyChanged(); }
        }

        private double averagePopulation = 0;
        public double AveragePopulation
        {
            get { return averagePopulation; }
            set { averagePopulation = value; NotifyChanged(); }
        }
        #endregion

        public VM()
        {
            //get a list of persons from the database
            cityList = db.Get();
            cities = new BindingList<City>(cityList);
        }
        //Sort list by city name
        public void SortbyCityName()
        {
            db.SortbyCityName(cityList);
            cities.ResetBindings();

        }

        public void SortbyPopulation()
        {
            db.SortbyPopulation(cityList);
            cities.ResetBindings();
        }

        public void Statistic()
        {
            int i = 0;
            TotalPopulation = 0;
            foreach(City city in Cities)
            {
                TotalPopulation += city.Population;
                i++;
            }
            AveragePopulation = Math.Round(TotalPopulation / i, 2);
            HighestPopulation = db.MaxPopulation();
            LowestPopulation = db.MinPopulation();
        }

        public void Delete()
        {
            if (SelectedCity != null)
            {
                db.Delete(SelectedCity);
                Cities.Remove(SelectedCity);
                Statistic();
            }
        }

        public void Save()
        {
            Cities.Add(CityToEdit);
            db.Add(CityToEdit);
            Statistic();
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

    }
}
