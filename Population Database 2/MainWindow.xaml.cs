﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Population_Database_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VM vm = new VM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            //create new person to edit
            vm.CityToEdit = new City();
            //create the edit window
            AddCity pw = new AddCity(vm)
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            pw.ShowDialog();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            vm.Delete();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vm.Statistic();
        }

        private void RdnSortbyCity_Checked(object sender, RoutedEventArgs e)
        {
            vm.SortbyCityName();
        }

        private void RdnSortbyPopulation_Checked(object sender, RoutedEventArgs e)
        {
            vm.SortbyPopulation();
        }
    }
}
