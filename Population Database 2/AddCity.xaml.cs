﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Population_Database_2
{
    /// <summary>
    /// Interaction logic for AddCity.xaml
    /// </summary>
    public partial class AddCity : Window
    {
        VM vm;
        public AddCity(VM vm)
        {
            InitializeComponent();
            DataContext = vm;
            //remember the vm class so we can call it in the save event handler
            this.vm = vm;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            //tell the vm class to save this and then close
            vm.Save();
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            //just close on cancel
            this.Close();
        }
    }
}
